import java.util.Scanner;

public class GraphStar 
{

	private static Scanner input;

	public static void main(String[] args)
	{
		input = new Scanner(System.in);
		
		String number;
				
		System.out.print("Please enter 5 numbers between 1 and 30 seperated by spaces\n");
					
		System.out.println("Those numbers represented in *");
		
		// for the 5 numbers entered by the user
		for (int i = 1; i<=5;i++)
		{
			number = input.next();
			int current = Integer.parseInt(number);
			
			//ensure they enter numbers from 1 to 30
			if (current<1||current>30)
			{
				System.out.print("Please enter numbers between 1 and 30");
				break;
			}
			
			// create * bar graphs
			for(int count=1; count<=current;count++)
			{
				System.out.print("*");
			}
			System.out.println();
		
		}

	}

}
